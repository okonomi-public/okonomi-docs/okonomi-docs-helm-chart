# Helm Chart for okonomi docs

## Introduction

This [Helm](https://github.com/kubernetes/helm) chart installs okonomi docs api server into a Kubernetes cluster.

## Prerequisites

- Kubernetes cluster 1.10+
- Helm 3.0.0+
- S3 compatible storage account
- PostgreSQL or MySQL Database

## Installation

### Add Pull Secrets

Okonomi docs images are only available through private docker registries. Therefore pull secrets need to be configured in the target namespace prior installation.

You can find the necessary steps in [Kubernetes Documentation: Pull an Image from a Private Registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).

### Add Helm repository

```bash
helm repo add okonomi-docs https://okonomi-public.gitlab.io/okonomi-docs/okonomi-docs-helm-chart
helm install my-okonomi-docs okonomi-docs/okonomi-docs -f values.yaml --version 1.0.0
```

### Configure the chart

The following items can be set via `--set` flag during installation or configured by providing a custom `values.yaml`. Database and storage credentials are prerequisites to run this chart.

## Configuration

The following table lists the configurable parameters of the okonomi-docs chart and the default values.

| Parameter                                                                   | Description                                                                                                        | Default                         |
| --------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------| ------------------------------- |
| **Image** |
|`image.repository` | okonomi-docs Image name ||                                        
| `image.pullPolicy` | okonomi-docs Image pull policy | IfNotPresent |
| `image.tag` | okonomi-docs Image tag |  |
| **Replica Count** |
| `replicaCount.web` | replicaCount for the `web` pod | `1` |
| `replicaCount.worker` | replicaCount for the `worker` pod | `1` |
| **S3** |
| `s3.enabled` | if `s3` should be enabled | `true` |
| `s3.region` | `region` setting for the s3 connection | |
| `s3.bucket` | `bucket` setting for the s3 connection | |
| `s3.secretKey` | `secretKey` setting for the s3 connection, provided directly to the chart | |
| `s3.secretKeySecret.name` | Name of the k8s secret to source the `secretKey` setting from | |
| `s3.secretKeySecret.key` | Key within the k8s secret to source the `secretKey` setting from | |
| `s3.accessKey` | `accessKey` setting for the s3 connection, provided directly to the chart | |
| `s3.accessKeySecret.name` | Name of the k8s secret to source the `accessKey` setting from | |
| `s3.accessKeySecret.key` | Key within the k8s secret to source the `accessKey` setting from | |
| **Database** |
| `database.adapter` | database adapter to use (`postgresql` or `mysql2`) | `postgresql` |
| `database.encoding` | database encoding to use | `unicode` |
| `database.host` | database host to use |  |
| `database.port` | databse port to use | `5432` |
| `database.name` | database name to use | `okonomi-docs` |
| `database.username` | `username` setting for the database connection, provided directly to the chart | |
| `database.usernameSecret.name` | Name of the k8s secret to source the `username` setting from | |
| `database.usernameSecret.key` | Key within the k8s secret to source the `username` setting from | |
| `database.password` | `password` setting for the database connection, provided directly to the chart | |
| `database.passwordSecret.name` | Name of the k8s secret to source the `password` setting from | |
| `database.passwordSecret.key` | Key within the k8s secret to source the `password` setting from | |
| **Rails** |
| `rails.secretKeyBase` | `secretKeyBase` setting for token encryption, provided directly to the chart | |
| `rails.secretKeyBaseSecret.name` | Name of the k8s secret to source the `secretKeyBase` setting from | |
| `rails.secretKeyBaseSecret.key` | Key within the k8s secret to source the `secretKeyBase` setting from | |
| `rails.maxThreads` | `maxThreads` setting for the web process | `5` |
| `rails.environment` | `environment` setting | `production` |
| `rails.logToStdout` | `logToStdout` setting | `enabled` |
| `rails.serveStaticFiles` | `serveStaticFiles` setting | `enabled` |
| **Autoscaling** |
| `autoscaling.enabled` | if `autoscaling` should be enabled | `false` |
